#################################################################################
# Bible Promise Program                                                         #
# Written By: Caleb Nelson - Dizzerin on gitlab and github                      #
# Language: Python                                                              #
# Date Created: 4/28/2019                                                       #
# Date Last Modified:                                                           #
# Description: A python program for viewing bible verses in different bible     #
# translation and saving, rating, tagging, and categorizing for later reference #
#################################################################################

# TODO: read about lambda functions

# TODO: Better support for multiple verses/sections
# TODO: implement looking up multiple verses differently (properly)
# TODO: support passages across chapter breaks - and input for such
# TODO: support multiple ranges of verses   9-11, 30-42

# TODO: more input sanitation and error handling
#           for choosing desired texts -- verify texts actually exist  I.e. not Psalms 999:999

# TODO: make sure I don't overwrite data when writing to excel files?
# TODO: implement support for multiple sheets in excel
# TODO: format excel file nicely with bold and borders etc. (implemented the frozen cell part!) working on column widths
# TODO: Support creating a new excel file from scratch which formats everything nicely
# TODO: figure out solution to openpyxl allowing max_row to be blank cells if they have been previously modified

#import Testing_Excel_File_Interaction
import BiblePromiseProgram_Functions as funct
import openpyxl as excel
from openpyxl.utils import column_index_from_string
from openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font, colors, Color

# Print welcome info
print("Welcome to my Bible Verse Lookup program!")
print("Written by: Caleb Nelson")
print("This program will lookup bible texts and return them to you")
print("You may choose to read bible texts to lookup from an excel file, or enter them here")
print("To begin, select a Bible translation from below.")

# Print bible translation options and get desired option
translations = [
    "KJV",
    "NKJV <-- currently unavailable(due to copyright issues), placed here as a placeholder",
    "NIV <-- currently unavailable(due to copyright issues), placed here as a placeholder",
    "NLT <-- currently unavailable(due to copyright issues), placed here as a placeholder"
]
# Get user selected translation
while True:
    print()
    print("Currently supported Bible translations: ")
    for i, translation in enumerate(translations, start=1):
        print("({}) {}".format(i, translations[i-1]))
    user_translation = input("Please select your desired translation: ")

    # If user input is blank, use default value of "KJV"
    if user_translation == "":
        user_translation = "KJV"
        break

    # Convert number to corresponding option
    try:
        user_translation = int(user_translation)  # The user entered a number

        # Verify it is a valid number option
        if user_translation <= 0 or user_translation > len(translations):
            print("Invalid option!  Please try again...")
            continue

        # Convert number to corresponding option
        user_translation = translations[user_translation - 1]

        # User input is good, break out of while loop
        break

    except ValueError:  # The user didn't enter a number - ignore failure to cast to int
        pass

    # Verify text is good
    if user_translation not in translations:
        print("Invalid option!  Please try again...")
    else:  # User input is good
        break  # Break out of while loop

# User option - process excel file or type in single verse
while True:
    print()
    print("How would you like to input texts to lookup?")
    print("(1) Process texts from excel file")
    print("(2) Enter text here")
    user_input_type = input("Please select your desired input type: ").strip()

    # Cast to int
    try:
        user_input_type = int(user_input_type)
    except ValueError:  # The user didn't enter a number - handle error
        print("Invalid input!  Please try again...")
        continue

    # Verify it is a valid number option
    if user_input_type == 1 or user_input_type == 2:
        break  # User input is good, break out of while loop
    else:
        print("Invalid option!  Please try again...")
        continue

# Initialize variables
prefix = None
book = ""
chapter = 3
verses = [16, 20, 30]

# Option 1 - Read desired verses from excel file
if user_input_type == 1:

    # Print welcome data
    print()
    print("You have chosen to process texts from an excel file.  This option will read the verses to lookup from one "
          "column in excel (starting on the second row) and paste the bible text in another column."
          "You can chose both columns.")

    # Get the name of the file to open
    workbook = ""
    while True:
        filename = input("Please enter the name of the excel file you would like to read from: ")

        # If input is blank - prompt user for filename again
        if filename == "":
            print("Blank input!  Please try again...")
            continue

        # Add .xlsx ending if it doesnt have it
        if filename[-5:] != ".xlsx" and filename[-4:] != ".xls":
            filename += ".xlsx"

        # Open the file, or handle errors if it could not be opened
        try:
            # Load the excel workbook
            # Note: data only parameter lets us get the data from cells and not the formulas in those cells
            workbook = excel.load_workbook(filename, data_only=True)
        except IOError as exception:
            print("Error, file could not be loaded!  Please check the file name or close it if its in use.")
            continue
        break

    # Get active excel sheet
    active_sheet = workbook.active

    # Get excel file column to read texts from
    while True:
        read_column = input("Enter the column letter to read texts from (default is column A): ").strip().upper()
        # If user input is blank, use default
        if read_column == "":
            read_column = "A"
            break
        # Verify that input
        if not read_column.isalpha():
            print("Invalid input, please try again...")
        else:
            break

    # Get excel file column to paste texts in
    while True:
        write_column = input("Enter the column letter to paste texts in (default is column B): ").strip().upper()
        # If user input is blank, use default
        if write_column == "":
            write_column = "B"
            break
        # Verify input
        if not write_column.isalpha():
            print("Invalid input, please try again...")
        else:
            break

    # Get excel row to start reading from
    while True:
        start_row = input("Enter the row number to start reading texts from (default is row 2): ").strip().upper()
        # If user input is blank, use default
        if start_row == "":
            start_row = 2
            break
        # Verify that input
        if not start_row.isdigit():
            print("Invalid input, please try again...")
        else:
            break
    # Freeze header row(s) about start row -- where the verses start, assume the row(s) above is/are title/header row(s)
    active_sheet.freeze_panes = active_sheet.cell(row=start_row, column=1)

    # Iterate over the chosen rows
    for i, row in enumerate(active_sheet.iter_rows(min_row=start_row, max_row=active_sheet.max_row,
                                                   min_col=column_index_from_string(read_column),
                                                   max_col=column_index_from_string(read_column)), start=start_row):
        for cell in row:
            # If cell is emtpy...
            if cell.value is None:
                # Write that the reference cell is empty
                active_sheet.cell(row=i, column=column_index_from_string(write_column)).value = "Reference cell empty"
                # Highlight cell yellow
                active_sheet.cell(row=i, column=column_index_from_string(write_column)).fill = \
                    PatternFill("solid", fgColor=colors.YELLOW)
                continue  # Continue on to next cell

            # Else print text being processed
            else:
                print("Processing input line: {}".format(cell.value))
                # Trim spaces and make it lower
                lookup_text = cell.value.strip().lower()

                try:
                    # Parse lookup text input
                    prefix, book, chapter, verses = funct.parse_input(lookup_text)

                    # Lookup verse
                    try:
                        # Write verse to chosen column
                        active_sheet.cell(
                            row=i,
                            column=column_index_from_string(write_column)
                        ).value = funct.get_verse(user_translation,
                                                  prefix,
                                                  book,
                                                  chapter,
                                                  verses)

                        # Clear any previous highlighting on cell
                        active_sheet.cell(row=i, column=column_index_from_string(write_column)).fill = PatternFill(None)

                    # If error querying API for text...
                    except Exception as exception:
                        # Write the error
                        active_sheet.cell(row=i, column=column_index_from_string(write_column)).value = \
                            "Text not found! Exception: {}".format(exception)
                        # Highlight cell green
                        active_sheet.cell(row=i, column=column_index_from_string(write_column)).fill = \
                            PatternFill("solid", fgColor=colors.GREEN)
                        continue  # Continue on to next row

                # If error parsing input...
                except Exception as exception:
                    # Write that the reference cell has bad input
                    active_sheet.cell(row=i, column=column_index_from_string(write_column)).value = \
                        "Reference cell contains bad input. Exception: {}".format(exception)
                    # Highlight cell red
                    active_sheet.cell(row=i, column=column_index_from_string(write_column)).fill = \
                        PatternFill("solid", fgColor=colors.RED)
                    continue  # Continue on to next row

    #TODO: set verse column widths to widest verse


    # Save the workbook
    workbook.save(filename)
    print("Successfully inserted texts and saved excel file!")


# Option 2 - Get desired verse or verse range from user
if user_input_type == 2:
    while True:
        print("Verses are accepted in the following formats:")
        print("Matthew 3:9")
        print("Matthew 3:9, 12")
        print("Matthew 3:9, 10, 12")
        print("Matthew 3:9-11")
        print("2 Corinthians 12:9       or  II Corinthians 12:9")
        print("2 Corinthians 12:9, 12   or  II Corinthians 12:9, 12")
        print("2 Corinthians 12:9, 12, 16   or  II Corinthians 12:9, 12")
        print("2 Corinthians 12:9-11    or  II Corinthians 12:9-11")
        # Note don't need spaces with commas
        user_lookup_text = input("Please enter the verse or verse range you would like to look up: ").strip().lower() # Trim spaces and make it lower

        # Parse lookup text input
        try:
            prefix, book, chapter, verses = funct.parse_input(user_lookup_text)
            break
        except Exception as exception:
            print(exception)
            continue  # Repeat prompt for input

    # Print the verse
    try:
        print(funct.get_verse(user_translation, prefix, book, chapter, verses))
    except Exception as exception:
        print(exception)
