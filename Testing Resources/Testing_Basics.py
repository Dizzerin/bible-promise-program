import matplotlib.pyplot as plt
import numpy
import os
import csv
import openpyxl
import urllib

print("this", "is", " a ", "test\n", "hmm")
# test
filePath = os.path.join("TextFile.txt")
print(filePath)
with open(filePath, "r+") as file:
    listOfLines = file.read()
    print(listOfLines)

filePath = os.path.join("ExcelFileCSV.csv")
with open(filePath, "r+", newline='') as file:
    listOfLines = csv.reader(file, delimiter=",")
    for index, row in enumerate(listOfLines, start=1):
        print("Index: ", index, " ".join(row))
    fileWriter = csv.writer(file, delimiter=",")
    # fileWriter.writerow(["test"] * 3 + ["more testing weeee"])

    toPrint = ["first", "second", "third"]
    for index, text in enumerate(toPrint, start=0):
        print(toPrint[index])
        fileWriter.writerow(text)
        fileWriter.writerow(toPrint)
        fileWriter.writerows(toPrint)

# x = numpy.arange(-10, 10, .01)
#
# y = 5 * numpy.sin(2 * numpy.pi * .5 * x)
#
# plt.plot(x, y)
# plt.title('My awesome plot')
# plt.xlabel('Time (s) $\Omega$')
# plt.ylabel('Amplitude')
# plt.show()


"""
* 2D lists
* functions
* file I/O
* validate input, handle errors well
* Other things to possibly have?
    * Branching
    * Looping
    * Console and file I/O
    * Pass by reference - Functions with various return and parameter types (default values, etc)
    * Globals
    * Vectors, passing 2D lists etc.
"""