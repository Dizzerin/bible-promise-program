import requests
import json
from xml.etree import cElementTree as ET

# could use this for NLT maybe?  -- http://api.nlt.to/


def retrieve_url(url):
    base_url = "https://api.scripture.api.bible"
    params = {
        "api-key": "0e889e6aeeca0a148063d8800eb31f96"
    }

    reply = requests.get(base_url + url, headers=params)

    somethingElse = json.loads(reply.text)

    return somethingElse['data']
    # return json.loads(requests.get(base_url + url, headers=params).text)['data']


# KJV bible ID
bibleId = "de4e12af7f28f599-02"


# bible_url = "/v1/bibles"

# bibles = retrieve_url(bible_url)

# bibleId = ""

# for bible in bibles:
#     if bible['name'] == 'King James (Authorised) Version' and bible['description'] == 'Protestant':
#         bibleId = bible['id']

book_url = "/v1/bibles/" + bibleId + "/books"

# books = retrieve_url(book_url)

# chapter_url = "/v1/bibles/" + bibleId + "/books/JHN/chapters"
#
# chapters = retrieve_url(chapter_url)
# for chapter in chapters:
#     print(chapter)

# verse_url = "/v1/bibles/" + bibleId + "/chapters/JHN.3/verses"
#
# verses = retrieve_url(verse_url)
#
# for verse in verses:
#     print(verse)

verse = retrieve_url('/v1/bibles/' + bibleId + '/verses/JHN.3.16')
# splits it into all child elements which in this case are only spans
soup = ET.fromstring(verse['content'])  # may only return first object element thing in list
textFromSoup = list(soup)[1].text
# some verses will have a paragraph symbol at the beginning, so we strip it and whitespace
strippedTextFromSoup = textFromSoup.strip("¶").strip()
print(strippedTextFromSoup)

# for elem in list(soup):
#     print(elem.find('span'))

# print(soup.find('span').text)

#
# names = [b['name'] for b in bibleList]
#
# for bible in bibleList:
#     print(bible['name'])
pass


# Sample of json that gets returned from API.Bible
# {
#     "data": {
#         "id": "JHN.3.16",
#         "orgId": "JHN.3.16",
#         "bookId": "JHN",
#         "chapterId": "JHN.3",
#         "bibleId": "de4e12af7f28f599-02",
#         "reference": "John 3:16",
#         "content": "<p class=\"p\"><span data-number=\"16\" class=\"v\">16</span><span class=\"wj\">¶ For God so loved the world, that he gave his only begotten Son, that whosoever believeth in him should not perish, but have everlasting life.</span> </p>",
#         "copyright": "PUBLIC DOMAIN except in the United Kingdom, where a Crown Copyright applies to printing the KJV. See http://www.cambridge.org/about-us/who-we-are/queens-printers-patent",
#         "next": {
#             "id": "JHN.3.17",
#             "number": "17"
#         },
#         "previous": {
#             "id": "JHN.3.15",
#             "number": "15"
#         }
#     },
#     "meta": {
#         "fums": "<script>\nvar _BAPI=_BAPI||{};\nif(typeof(_BAPI.t)==='undefined'){\ndocument.write('\\x3Cscript src=\"'+document.location.protocol+'//cdn.scripture.api.bible/fums/fumsv2.min.js\"\\x3E\\x3C/script\\x3E');}\ndocument.write(\"\\x3Cscript\\x3E_BAPI.t('' + unique + '');\\x3C/script\\x3E\");\n</script><noscript><img src=\"https://d3a2okcloueqyx.cloudfront.net/nf1?t=' + unique + '\" height=\"1\" width=\"1\" border=\"0\" alt=\"\" style=\"height: 0; width: 0;\" /></noscript>",
#         "fumsId": "479b8182-e6e6-4d49-b67b-50a284a18a2e",
#         "fumsJsInclude": "cdn.scripture.api.bible/fums/fumsv2.min.js",
#         "fumsJs": "var _BAPI=_BAPI||{};if(typeof(_BAPI.t)!='undefined'){ _BAPI.t('479b8182-e6e6-4d49-b67b-50a284a18a2e'); }",
#         "fumsNoScript": "<img src=\"https://d3btgtzu3ctdwx.cloudfront.net/nf1?t=479b8182-e6e6-4d49-b67b-50a284a18a2e\" height=\"1\" width=\"1\" border=\"0\" alt=\"\" style=\"height: 0; width: 0;\" />"
#     }
# }