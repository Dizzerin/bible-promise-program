import requests
import json
from xml.etree import cElementTree as ET

# NOTES:
# Currently only support KJV - will soon expand to support other versions from https://api.scripture.api.bible
# Will later expand to include NLT from http://api.nlt.to/
# Would like to support NIV - but it appears to have strict copyright protections

# Note: May switch to use this API, it looks like it may be easier to use, certianly for dealing with quotes https://bible-api.com/

# TODO: Implement passage/section retrieving
# TODO: support multiple verses and multiple chapters (properly)
# TODO: Add option to include verse numbers vs ignore them
# TODO: fix more space problems

# Note: Use /passages/ rather than /verses/ and use the format
# BOOK.CHA.VERS-BOOK.CHA.VERS.
# Sample: https://api.scripture.api.bible/v1/bibles/[id]/passages/PSA.1.1-PSA.1.2.

# Temp code to get bible book IDs dictionary
# book_names_list = retrieve_url("/v1/bibles/de4e12af7f28f599-02/books") # Returns a list of dictionaries
# for book in book_names_list:
#     print("\"" + book["name"].lower() + "\"" + ": " + "\"" + book["id"] + "\",")


book_IDs = {
    "genesis": "GEN",
    "exodus": "EXO",
    "leviticus": "LEV",
    "numbers": "NUM",
    "deuteronomy": "DEU",
    "joshua": "JOS",
    "judges": "JDG",
    "ruth": "RUT",
    "1 samuel": "1SA",
    "2 samuel": "2SA",
    "1 kings": "1KI",
    "2 kings": "2KI",
    "1 chronicles": "1CH",
    "2 chronicles": "2CH",
    "ezra": "EZR",
    "nehemiah": "NEH",
    "esther": "EST",
    "job": "JOB",
    "psalms": "PSA",
    "proverbs": "PRO",
    "ecclesiastes": "ECC",
    "song of solomon": "SNG",
    "isaiah": "ISA",
    "jeremiah": "JER",
    "lamentations": "LAM",
    "ezekiel": "EZK",
    "daniel": "DAN",
    "hosea": "HOS",
    "joel": "JOL",
    "amos": "AMO",
    "obadiah": "OBA",
    "jonah": "JON",
    "micah": "MIC",
    "nahum": "NAM",
    "habakkuk": "HAB",
    "zephaniah": "ZEP",
    "haggai": "HAG",
    "zechariah": "ZEC",
    "malachi": "MAL",
    "matthew": "MAT",
    "mark": "MRK",
    "luke": "LUK",
    "john": "JHN",
    "acts": "ACT",
    "romans": "ROM",
    "1 corinthians": "1CO",
    "2 corinthians": "2CO",
    "galatians": "GAL",
    "ephesians": "EPH",
    "philippians": "PHP",
    "colossians": "COL",
    "1 thessalonians": "1TH",
    "2 thessalonians": "2TH",
    "1 timothy": "1TI",
    "2 timothy": "2TI",
    "titus": "TIT",
    "philemon": "PHM",
    "hebrews": "HEB",
    "james": "JAS",
    "1 peter": "1PE",
    "2 peter": "2PE",
    "1 john": "1JN",
    "2 john": "2JN",
    "3 john": "3JN",
    "jude": "JUD",
    "revelation": "REV"
}


def parse_input(user_input):
    # Parse that input
    prefix = None
    book = "John"
    chapter = 3
    verses = [16, 20, 30]

    split = user_input.split(" ")

    # Make sure this has at least two elements, a book part and a chapter part
    if len(split) < 2:
        raise Exception("Error, improper format, no book and chapter/verse part")

    # If there is a book prefix, parse it properly
    if user_input[0].isnumeric() or user_input[0] == "i" and (user_input[1] == "i" or user_input[1] == " "):
        prefix = split[0]
        if prefix.isdigit():
            prefix = int(prefix)
        else:
            # Count number of i's in string
            prefix = prefix.count("i")
        split = split[1:]

    # Iterate over list and find the first one that starts with numeric value (for book and verse sections)
    for index, string in enumerate(split):
        if string[0].isdigit():
            # Make sure index is not 0
            if index == 0:
                raise Exception("invalid input")
            # Parse book
            book_part = split[:index]
            # Join it
            book = " ".join(book_part)
            # Parse verses - and join it and get rid of spaces
            chapter_verse_part = ''.join(split[index:])
            # Check to make sure there is a colon
            if chapter_verse_part.find(":") == -1:
                raise Exception("Improper format, no chapter verse divider!")
            # Split at : and take the first part as the chapter
            chapter_part_split = chapter_verse_part.split(":")
            # Make sure there is something on both sides of the :
            if len(chapter_verse_part) < 2:
                raise Exception("No chapter or verses provided")
            # Get the chapter
            chapter = int(chapter_part_split[0])

            verses_unparsed = chapter_part_split[1]  # This is a single string with everything after the :
            if verses_unparsed.find("-") != -1:
                verses_split = verses_unparsed.split("-")
                # Make sure the length (of the resulting list) is two
                # (so there is only 1 dash and there is a number on both sides of the -
                # NOTE: would need to change this section if I wanted to be able to deal with multiple passage sections
                # I.e. verses 9-11, 1-16
                if len(verses_split) != 2:
                    raise Exception("No number on either side of a dash or multiple dashes in verse part")
                # Make sure the numbers are numbers
                try:
                    n1 = int(verses_split[0])
                    n2 = int(verses_split[1])
                except ValueError:
                    raise Exception("Verse was not a number")
                # and make sure that they are in ascending order
                if n1 > n2:
                    raise Exception("Error, verses not in ascending order")
                # Parse verses into list using list comprehension
                verses = [verse for verse in range(n1, n2+1)]
            else:  # Case with commas for verses
                verses_split = verses_unparsed.split(",")
                # Take only the numbers in the verses_split and cast them to ints and store them in a list
                verses = [int(verse) for verse in verses_split if verse.isdigit()]
            break  # Break at the first digit

    # # User input is good, print it for verification and return it
    # print("Prefix: " + str(prefix))
    # print("Book: " + book)
    # print("Chapter: " + str(chapter))
    # print("Verses: ", end="")
    # for verse in verses:
    #     print(str(verse), end=" ")
    # print()

    return [prefix, book, chapter, verses]


def retrieve_url(url):
    base_url = "https://api.scripture.api.bible"
    params = {
        "api-key": "0e889e6aeeca0a148063d8800eb31f96"
    }
    reply = requests.get(base_url + url, headers=params)
    rename_me = json.loads(reply.text)
    return rename_me['data']
    # return json.loads(requests.get(base_url + url, headers=params).text)['data']


def get_verse(version, prefix, book, chapter, verses):
    # TODO: support more bible versions

    if version.upper() == "KJV":
        # KJV bible ID
        bible_id = "de4e12af7f28f599-02"

        # In this case prefix and book part should be combined if prefix is not None
        if prefix is not None:
            book = str(prefix) + " " + book
        # Convert book to proper book ID
        book_id = book_IDs[book]

        # Todo: change below implementation of multiple verses, because there is a much better way
        text_to_return = ""
        for verse in verses:
            # Query web and get verse
            raw_data = retrieve_url('/v1/bibles/' + bible_id + '/verses/' + book_id + '.' + str(chapter) + '.' + str(verse))
            # splits raw XML data into all its child elements
            soup = ET.fromstring(raw_data['content'])  # may only return first object element thing in list
            parse_soup = list(soup)
            for element in parse_soup:

                # Deal with opening quote cases
                if element.attrib["class"] == "wj":
                    # Check to see if the end of the string currently has quotes
                    if text_to_return[-1:] == "\"":
                        # If so, remove the ending quotes (and don't add more) - because we are continuing a quote
                        text_to_return = text_to_return[:-1]
                    else:
                        # Add opening quote
                        text_to_return += "\""

                # Add text if it exists and is not a verse number
                if element.text is not None and not element.text.isdigit():
                    # Strip paragraph symbol if it is there
                    element.text = element.text.strip("¶")
                    # Only strip leading whitespace if its in a quote and the last character is a quote
                    if element.attrib["class"] == "wj" and text_to_return[-1:] == "\"":
                        # Strip leading whitespace before opening quote
                        element.text = element.text.lstrip()

                        # If appending to existing string and last character is not a space and not a quote
                        # and what is being appended doesn't start with a space, add a space
                        if len(text_to_return) > 0 and text_to_return[-1:] != "\"" and text_to_return[-1:] != " " and \
                                element.text[0] != " ":
                            text_to_return += " "

                    text_to_return += element.text

                # Add tail if it exists and is not a verse number
                if element.tail is not None and not element.tail.isdigit():
                    # If appending to existing string and last character is not a space and not a quote
                    # and what is being appended doesn't start with a space, add a space
                    if len(text_to_return) > 0 and text_to_return[-1:] != "\"" and text_to_return[-1:] != " " and \
                            element.text[0] != " ":
                        text_to_return += " "

                    # Strip paragraph symbol if its there
                    text_to_return += element.tail.strip("¶")

                # Deal with closing quotes
                if element.attrib["class"] == "wj":
                    # Strip trailing whitespace before closing quote and paragraph symbol if its there
                    text_to_return = text_to_return.strip("¶").rstrip()
                    text_to_return += "\""

        # Return text
        return text_to_return

    else:
        raise Exception("Error, unsupported bible version requested! The value of \"version\" was: {}".format(version))
