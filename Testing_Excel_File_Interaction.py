import openpyxl as excel

"""
great reference sources:
https://automatetheboringstuff.com/chapter12/
http://zetcode.com/articles/openpyxl/
https://openpyxl.readthedocs.io/en/stable/

Still need to figure out how to:
* format cells: bold, border etc
* move data around

Note: should also always check to see if a cell is empty before trying to print it or else it will error
"""

# set file path
filepath="SavedName.xlsx"

# Load the workbook
# Note: data only parameter lets us get the data from cells and not the formulas in those cells
workbook = excel.load_workbook(filepath, data_only=True)

# Get active sheet
sheet = workbook.active

# Get sheet name
sheetName = sheet.title
print("Active sheet is named: " + sheetName)

# Create a new sheet
workbook.create_sheet("New Sheet")

# Get sheet by name
newSheet = workbook.get_sheet_by_name("New Sheet")

# Set tab color of that new sheet
newSheet.sheet_properties.tabColor = "0072BA"

# Check to see if a sheet exists
if 'Sheet2' in workbook.sheetnames:
    # Delete a sheet
    workbook.remove_sheet(workbook.get_sheet_by_name("Sheet2"))

########################################## Writing Data ##########################################

# Write integer value
sheet['A2'] = 33

# Write string value
sheet["B3"] = "test"

# Write value another way
sheet.cell(row=2, column=3).value = "test2"

# Append values at the bottom of the current sheet - put it all in one row, but in separate columns
sheet.append([1, 2, 3])
sheet.append(("test", "test2", "test3"))

# Append multiple values from a 2D list
rowsToAppend = (
    (1,),
    (1, 2),
    (1, 2, 3),
    (1, 2, 3, )
)
for row in rowsToAppend:
    sheet.append(row)


########################################## Reading Data ##########################################

# Read and print contents of some cells in some different ways
a1 = sheet['A1']
print("The contents of A1 is: " + a1.value)
print("The contents of B1 is: " + sheet['B1'].value)
c3 = sheet.cell(row=3, column=3)
print("The contents of C3 is: " + c3.value)

# Read and print a range of cells
cellRange = sheet['A1' : 'C3']
# For those three elements in a tuple...  (you could also just use one variable and index off of it)
for c1, c2, c3 in cellRange:
    # Messing around with fancy formatting
    print("{0:8} {1:8} {2:8}".format(c1.value, c2.value, c3.value))

############# Dimensions ################
# Get the dimensions of the area of nonempty cells
# Returns the value of the top left cell with data and bottom right cell with data
print("sheet " + sheet.title + " dimensions:" + sheet.dimensions)

# Get max row count - useful for getting the dimensions of non empty cells
# min_row and max_row return the uppermost and lowermost rows with data
maxRowCount = sheet.max_row

# Get max column count - useful for getting the dimensions of non empty cells
# min_col and max_col return the leftmost and rightmost cols with data
maxColumnCount = sheet.max_column


# Set variable equal to a cell
cellD3 = sheet["D3"]
# Print the row the cell is in - returns an int, so cast to a string
print(str(cellD3.row))
# Print the column the cell is in
print(cellD3.column)
# Print the full coordinate of the cell, in this case its "B3"
print(cellD3.coordinate)
# Print the value of the cell
print(cellD3.value)

#### Note:  You can also convert between column letters and numbers  ###
#
# get_column_letter(27)
# 'AA'
# column_index_from_string('AA')
# 27
#
# Also:
# xy = coordinate_from_string('A4') # returns ('A',4)
# col = column_index_from_string(xy[0]) # returns 1
#
# can also convert to absolute coordinates:
# from --> openpyxl.utils.cell.absolute_coordinate(coord_string)
# Convert a coordinate to an absolute coordinate string (B12 -> $B$12)

# Testing -- apparently this is some sort of object
whatIsThis = sheet.column_dimensions
print("What is this = " + str(whatIsThis))


####################### More Reading (Reading Ranges of Cells) ##########################

# Iterate over all cells
# Iterate over all rows
for i in range(1, maxRowCount + 1):
    # Iterate over all columns
    for j in range(1, maxColumnCount + 1):
        # Get particular cell value
        currentCell = sheet.cell(row=i, column=j)

        # Print a space if cell is emtpy
        if currentCell.value is None:
            print(" ")
        # Else print cell contents
        else:
            print(currentCell.value, end=' | ')

    # Print new line
    print('\n')


####### More Reading - Iterating by Rows and By Columns ############
# Another way to iterate over stuff

rows = (
    (88, 46, 57),
    (89, 38, 12),
    (23, 59, 78),
    (56, 21, 98),
    (24, 18, 43),
    (34, 15, 67)
)

for row in rows:
    sheet.append(row)

# Iterate by rows
for row in sheet.iter_cols(min_row=1, min_col=1, max_row=6, max_col=3):
    for cell in row:
        print(cell.value, end=" ")
    print()

# Iterate by columns
for row in sheet.iter_cols(min_row=1, min_col=1, max_row=6, max_col=3):
    for cell in row:
        print(cell.value, end=" ")
    print()

########################### Hide Areas ###########################
# This hides columns or rows - creates an expandable and collapsible group
newSheet.column_dimensions.group('A','D', hidden=True)
newSheet.row_dimensions.group(1,10, hidden=False)

########################### Insert and Delete Rows ###########################
# Insert row at the top (inserts before row 1)
sheet.insert_rows(1)
# Insert 3 columns at the left (inserts before column A)
sheet.insert_cols(1, 3)
# Delete the last column just added (now column C or 3)
sheet.delete_cols(3)

########################### Merge and Unmerge Cells ###########################
# Merge cells A1:C1
newSheet.merge_cells('A1:C1')
# Put text in merged cells
newSheet['A1'] = "These should be merged"
# Merge cells B2:D3 a different way
newSheet.merge_cells(start_row=2, start_column=2, end_row=3, end_column=4)
# Merge and unmerge B4:D5
newSheet.merge_cells('B4:D5')
newSheet.unmerge_cells('B4:D5')

########################### Freezing Panes ###########################
newSheet.freeze_panes = 'B2'

####################### Moving Data Around #####################


#################### Formatting Cells - Bold, Borders, Fonts, Colors, Fills, Strikethrough, Column Widths etc. ###########
# Read https://openpyxl.readthedocs.io/en/stable/styles.html
# https://stackoverflow.com/questions/8440284/setting-styles-in-openpyxl

# Bold cell contents  -- NEEDS MORE TESTING -- none of these worked
sheet["D2"].font.copy(bold=True)
# sheet["D3"].font = sheet["D3"].font(bold=True)
# sheet["D4"].style.font.bold = True

# # Font properties
# _cell.style.font.color.index = Color.GREEN
# _cell.style.font.name = 'Arial'
# _cell.style.font.size = 8
# _cell.style.font.bold = True
# _cell.style.alignment.wrap_text = True
#
# # Cell background color
# _cell.style.fill.fill_type = Fill.FILL_SOLID
# _cell.style.fill.start_color.index = Color.DARKRED
#
# # You should only modify column dimensions after you have written a cell in
# #     the column. Perfect world: write column dimensions once per column
# #
# ws.column_dimensions["C"].width = 60.0


# Save the workbook
workbook.save(filepath)

print("Successfully completed excel file interaction testing")


# Other
# list comprehension test - basically performs the desired operation on every element in the list
strings = [
    '   HI HOW ARE YOU   \n',
    '   HI HOW ARE YOU1   \n',
    '   HI HOW ARE YOU2   \n',
    '   HI HOW ARE YOU3   \n'
]
# list comprehension test - basically performs the desired operation on every element in the list
strings = [string.strip().lower() for string in strings]